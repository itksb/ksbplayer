#!/usr/bin/env bash

# Fail on any error
set -o errexit


USER_ID=$(id -u)
GROUP_ID=$(id -g)

docker run -it --rm -p3000:3000 -v `pwd`:/app -w /app --user=${USER_ID}:${GROUP_ID} node:8 bash
